package cs.mad.flashcards.adapters

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao
import kotlinx.android.synthetic.main.item_flashcard_set.view.*
import cs.mad.flashcards.databinding.ItemFlashcardSetBinding

class FlashcardSetAdapter(private var flashcardSets: List<FlashcardSet>, private val dao: FlashcardSetDao) :
        RecyclerView.Adapter<FlashcardSetAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemFlashcardSetBinding) : RecyclerView.ViewHolder(binding.root)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemFlashcardSetBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = flashcardSets[position]
        holder.binding.infoText.text = item.title
        holder.itemView.setOnClickListener {
            val context = holder.itemView.context
            val showFlashcardSetIntent = Intent(context, FlashcardSetDetailActivity::class.java)
            showFlashcardSetIntent.putExtra("FLASHCARDSETID", item.id)
            context.startActivity(showFlashcardSetIntent)
        }
    }

    // return the size of the data set
    override fun getItemCount(): Int {
        return flashcardSets.size
    }

    fun addNewItem(newItem: FlashcardSet) {
//        flashcardSets.add(newItem)
//        notifyItemInserted(itemCount - 1)
    }

    fun setData(items: List<FlashcardSet>) {
        flashcardSets = items
        notifyDataSetChanged()
    }
}