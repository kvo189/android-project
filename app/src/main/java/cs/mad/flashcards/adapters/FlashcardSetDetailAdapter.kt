package cs.mad.flashcards.adapters

import android.app.AlertDialog
import android.content.DialogInterface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.MainActivity.Companion.applicationContext
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao
import kotlinx.android.synthetic.main.item_flashcard_set.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FlashcardSetDetailAdapter(
    private var flashcards: List<Flashcard>,
    private val flashcardDao: FlashcardDao
): RecyclerView.Adapter<FlashcardSetDetailAdapter.ViewHolder>() {

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener, View.OnLongClickListener {
        private var view: View = v
        private lateinit var flashcard: Flashcard
        private var flashcardPos: Int = 0
        private lateinit var flashcardDao: FlashcardDao

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        private val editButtonClick = { _: DialogInterface, _: Int ->
//            Toast.makeText(applicationContext(), android.R.string.ok, Toast.LENGTH_LONG).show()
            withEditText(v)
        }
        private val negativeButtonClick = { _: DialogInterface, _: Int ->
//            Toast.makeText(applicationContext(), android.R.string.cancel, Toast.LENGTH_SHORT).show()
        }

        private fun withEditText(view: View) {
            val builder = AlertDialog.Builder(itemView.context)
            val inflater = LayoutInflater.from(applicationContext())
            //builder.setTitle("With EditText")
            val dialogLayout = inflater.inflate(R.layout.alert_dialog_with_edittext, null)
            val editTitle = dialogLayout.findViewById<EditText>(R.id.editTitle)
            val editText  = dialogLayout.findViewById<EditText>(R.id.editText)
            editTitle.setText(flashcard.term)
            editText.setText(flashcard.definition)
            builder.setView(dialogLayout)
            builder.setPositiveButton("DONE") { _, _ ->
                Toast.makeText(
                    applicationContext(),
                    "'${editTitle.text}' is '${editText.text}'",
                    Toast.LENGTH_SHORT
                ).show()
                updateFlashcard(flashcard, editTitle.text.toString(), editText.text.toString())
            }
            builder.setNegativeButton("DELETE") { _, _ ->
                Toast.makeText(
                    applicationContext(),
                    "'${editTitle.text}' is Deleted",
                    Toast.LENGTH_SHORT
                ).show()
                remove(flashcard)
            }
            builder.show()
        }



        override fun onClick(v: View?) {
            Log.d("FlashcardSetDetailAdapter", "CLICK!")
            val builder = AlertDialog.Builder(itemView.context)
            builder.setTitle(flashcard.term)
            builder.setMessage(flashcard.definition)
            builder.setPositiveButton("EDIT", DialogInterface.OnClickListener(editButtonClick))
            builder.setNegativeButton("CANCEL", negativeButtonClick)
            builder.show()
        }

        override fun onLongClick(v: View?): Boolean {
            if (v != null) {
                withEditText(v)
            }
            return true
        }

        fun bindFlashcard(flashcard: Flashcard, position: Int, flashcardDao: FlashcardDao) {
            this.flashcard = flashcard
            this.flashcardPos = position
            this.flashcardDao = flashcardDao
            view.info_text.text = flashcard.term
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_flashcard, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemFlashcard = flashcards[position]
        holder.bindFlashcard(itemFlashcard, position, flashcardDao)
    }

    override fun getItemCount(): Int {
        return flashcards.size
    }

    fun getFlashCards(): MutableList<Flashcard> {
        return mutableListOf()
    }

    fun addNewItem(itemsNew: Flashcard){
//        flashcards.add(itemsNew)
//        notifyItemInserted(itemCount - 1)
    }

    fun updateFlashcard(flashcard: Flashcard, term: String, def: String){
        GlobalScope.launch {
            flashcard.term = term
            flashcard.definition = def
            flashcardDao.update(flashcard)

            GlobalScope.launch(Dispatchers.Main) {
                setData(flashcardDao.getAll())
            }
        }
    }

    private fun remove(flashcard: Flashcard) {
        GlobalScope.launch {
            flashcardDao.delete(flashcard)

            GlobalScope.launch(Dispatchers.Main) {
                setData(flashcardDao.getAll())
            }
        }
    }

    fun setData(items: List<Flashcard>) {
        flashcards = items
        notifyDataSetChanged()
    }
}