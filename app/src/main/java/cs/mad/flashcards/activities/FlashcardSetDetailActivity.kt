package cs.mad.flashcards.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import cs.mad.flashcards.AppDatabase
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.adapters.FlashcardSetDetailAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import kotlinx.android.synthetic.main.activity_flashcard_set_detail.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: FlashcardSetDetailAdapter
    private lateinit var flashcardSet: FlashcardSet
    private val flashcardDao by lazy { AppDatabase.getDatabase(applicationContext).flashcardDao() }
    private val flashcardSetDao by lazy { AppDatabase.getDatabase(applicationContext).flashcardSetDao() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard_set_detail)

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView2.layoutManager = linearLayoutManager

        adapter = FlashcardSetDetailAdapter(listOf(), flashcardDao)
        recyclerView2.adapter = adapter
        loadFromDb()

        val flashCardSetTitle: TextView = findViewById(R.id.flashcard_set_title)
        val flashcardSetID = intent.getIntExtra("FLASHCARDSETID", -1)

        lifecycleScope.launch {
            flashcardSet = flashcardSetDao.loadSingle(flashcardSetID)
            flashCardSetTitle.text = flashcardSet.title
//            Log.v("FlashCardSetDetailActivity", flashcardSet.id.toString())
        }

    }

    private fun loadFromDb() {
        lifecycleScope.launch {
            (recyclerView2.adapter as FlashcardSetDetailAdapter).setData(flashcardDao.getAll())
        }
    }

    fun onAddButtonClicked(view: View) {
        lifecycleScope.launch {
            flashcardDao.insert(Flashcard(term = "New Card", definition = "New def"))
            loadFromDb()
        }
    }

    fun onStudyButtonClicked(view: View) {
        val showStudySetActivity = Intent(this, StudySetActivity::class.java)
        startActivity(showStudySetActivity)
    }

    fun onDeleteButtonClicked(view: View) {
        lifecycleScope.launch {
            flashcardSetDao.delete(flashcardSet)
        }
        finish()
    }
}