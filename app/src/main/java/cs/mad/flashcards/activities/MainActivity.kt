package cs.mad.flashcards.activities

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.room.Room
import cs.mad.flashcards.AppDatabase
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao
import androidx.lifecycle.lifecycleScope
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch
import cs.mad.flashcards.databinding.ActivityMainBinding
import kotlinx.coroutines.GlobalScope
import java.util.*

//GIT Commit
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var adapter: FlashcardSetAdapter
    private lateinit var flashcardSets: List<FlashcardSet>
    private lateinit var db: AppDatabase
    private val flashcardSetDao by lazy { AppDatabase.getDatabase(applicationContext).flashcardSetDao() }

    init {
        instance = this
    }

    companion object {
        private var instance: MainActivity? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gridLayoutManager = GridLayoutManager(this, 2)
        recyclerView.layoutManager = gridLayoutManager

        binding.recyclerView.adapter = FlashcardSetAdapter(listOf(), flashcardSetDao)
        loadFromDb()
    }

    override fun onResume() {
        super.onResume()
        loadFromDb()
    }

    private fun loadFromDb() {
        lifecycleScope.launch {
            (recyclerView.adapter as FlashcardSetAdapter).setData(flashcardSetDao.getAll())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add, menu)
        return true
    }

    fun onAddButtonClicked1(menuItem: MenuItem) {
        lifecycleScope.launch {
            flashcardSetDao.insertFlashcardSet(FlashcardSet(title = "New Set"))
            loadFromDb()
        }
    }
}