package cs.mad.flashcards.activities

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GestureDetectorCompat
import cs.mad.flashcards.AppDatabase
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard
import kotlinx.android.synthetic.main.activity_study_set.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class StudySetActivity : AppCompatActivity() {
    private var flashcards: MutableList<Flashcard> = mutableListOf()
    private lateinit var flashcardsMissed: MutableList<Boolean>
    private var totalFlashcards: Int = 0
    private var missed: Int = 0
    private var correct: Int = 0
    private var completed: Int = 0
    private val flashcardDao by lazy { AppDatabase.getDatabase(applicationContext).flashcardDao() }

    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_set)

        //get flashcards from dao first then configure the activity
        GlobalScope.launch {
            flashcards = flashcardDao.getAll().toMutableList()

            GlobalScope.launch (Dispatchers.Main ){
                flashcardsMissed = MutableList(flashcards.size) {_ -> false}
                totalFlashcards = flashcards.size

                //display button texts
                if (flashcards.count() > 0) {
                    buttonFlashCard.text = flashcards.first().term
                }else{
                    buttonFlashCard.text = getString(R.string.flash_card_set_empty)
                }
                textViewMissed.text = getString(R.string.missed_count, missed.toString())
                textViewCorrect.text = getString(R.string.correct_count, correct.toString())
                textViewCompleted.text = getString(R.string.completed_count, correct.toString(), totalFlashcards.toString())

                //handle onTouchListener
                buttonFlashCard.setOnTouchListener(object: OnSwipeTouchListener(this@StudySetActivity) {
                    override fun onClick() {
                        super.onClick()
                        flipFlashcard()
                    }

                    override fun onSwipeLeft() {
                        super.onSwipeLeft()
                        Toast.makeText(this@StudySetActivity, "Swipe Left gesture detected",Toast.LENGTH_SHORT).show()
                        markIncorrect()
                    }
                    override fun onSwipeRight() {
                        super.onSwipeRight()
                        Toast.makeText(this@StudySetActivity,"Swipe Right gesture detected",Toast.LENGTH_SHORT).show()
                        markCorrect()
                    }

                    override fun onSwipeTop() {
                        super.onSwipeTop()
                        Toast.makeText(this@StudySetActivity,"Swipe Up gesture detected",Toast.LENGTH_SHORT).show()
                        skip()
                    }
                })

                buttonExit.setOnClickListener {
                    finish()
                }

                buttonSkip.setOnClickListener {
                    skip()
                }

                buttonMissed.setOnClickListener {
                    markIncorrect()
                }

                buttonCorrect.setOnClickListener {
                    markCorrect()
                }
            }
        }

    }

    private fun flipFlashcard(){
        if (flashcards.isEmpty()) {return}
        if (buttonFlashCard.text == flashcards[0].term){
            buttonFlashCard.text = flashcards[0].definition
        }else{
            buttonFlashCard.text = flashcards[0].term
        }
    }

    @ExperimentalStdlibApi
    private fun skip(){
        if (flashcards.isNotEmpty()) {
            flashcards.add(flashcards.removeFirst())
            flashcardsMissed.add(flashcardsMissed.removeFirst())
            buttonFlashCard.text = flashcards.first().term
        }
    }

    @ExperimentalStdlibApi
    private fun markIncorrect(){
        if (flashcards.isNotEmpty()) {
            flashcards.add(flashcards.removeFirst())
            flashcardsMissed[0] = true
            flashcardsMissed.add(flashcardsMissed.removeFirst())
            buttonFlashCard.text = flashcards.first().term
            missed++
            textViewMissed.text = getString(R.string.missed_count, missed.toString())
        }
    }

    @ExperimentalStdlibApi
    private fun markCorrect(){
        if (flashcards.isNotEmpty()) {
            if (!flashcardsMissed[0]) {
                correct++
                textViewCorrect.text = getString(R.string.correct_count, correct.toString())
            }
            completed++
            textViewCompleted.text = getString(R.string.completed_count, completed.toString(), totalFlashcards.toString())

            flashcards.removeFirst()
            flashcardsMissed.removeFirst()

            if (flashcards.isEmpty()) {
                buttonFlashCard.text = getString(R.string.flash_card_set_empty)
            }else{
                buttonFlashCard.text = flashcards.first().term
            }
        }
    }
}

open class OnSwipeTouchListener(context: Context?) : View.OnTouchListener {

    private val gestureDetector: GestureDetector

    companion object {

        private const val SWIPE_THRESHOLD = 100
        private const val SWIPE_VELOCITY_THRESHOLD = 100
    }

    init {
        gestureDetector = GestureDetector(context, GestureListener())
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        return gestureDetector.onTouchEvent(event)
    }

    private inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            onClick()
            return super.onSingleTapUp(e)
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            var result = false
            try {
                val diffY = e2.y - e1.y
                val diffX = e2.x - e1.x
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight()
                        } else {
                            onSwipeLeft()
                        }
                        result = true
                    }
                } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom()
                    } else {
                        onSwipeTop()
                    }
                    result = true
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
            }

            return result
        }
    }

    open fun onSwipeRight() {}

    open fun onSwipeLeft() {}

    open fun onSwipeTop() {}

    open fun onSwipeBottom() {}

    open fun onClick() {}
}