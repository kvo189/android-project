package cs.mad.flashcards.entities

import androidx.room.*

@Dao
interface FlashcardSetDao {
    @Query("SELECT * FROM flashcardSets")
    suspend fun getAll(): List<FlashcardSet>

    @Query("SELECT * FROM flashcardSets WHERE id IN (:flashcardSetIds)")
    fun loadAllByIds(flashcardSetIds: IntArray): List<FlashcardSet>

    @Query("SELECT * FROM flashcardSets WHERE id =:id")
    suspend fun loadSingle(id: Int): FlashcardSet

    @Query("SELECT * FROM flashcardSets WHERE title LIKE :title")
    fun findByTitle(title: String): FlashcardSet

    @Insert
    fun insertAll(vararg flashcardSets: FlashcardSet)

    @Insert //(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFlashcardSet(flashcardSet: FlashcardSet)

    @Delete
    suspend fun delete(flashcardSet: FlashcardSet)
}