package cs.mad.flashcards.entities

import android.util.Log
import androidx.room.*

@Entity(tableName = "flashcardSets")
data class FlashcardSet(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val title: String
    )