package cs.mad.flashcards.entities

import androidx.room.*

@Entity(tableName = "flashcards")
data class Flashcard(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    var term: String,
    var definition: String
)

@Dao
interface FlashcardDao {

    @Query("SELECT * FROM flashcards")
    suspend fun getAll(): List<Flashcard>

    @Query("SELECT * FROM flashcards WHERE id IN (:flashcardIds)")
    suspend fun loadAllByIds(flashcardIds: IntArray): List<Flashcard>

    @Query("SELECT * FROM flashcards WHERE term LIKE :term")
    suspend fun findByTerm(term: String): Flashcard

    @Insert
    suspend fun insertAll(vararg flashcards: Flashcard)

    @Insert //(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(flashcard: Flashcard)

    @Update
    suspend fun update(flashcard: Flashcard)

    @Delete
    suspend fun delete(flashcard: Flashcard)
}

//fun tenFlashcards (): List<Flashcard> {
//    val flashcards = mutableListOf(
//        Flashcard("Flashcard 1 term", "Default definition"),
//        Flashcard("Flashcard 2 term", "Default definition"),
//        Flashcard("Flashcard 3 term", "Default definition"),
//        Flashcard("Flashcard 4 term", "Default definition"),
//        Flashcard("Flashcard 5 term", "Default definition"),
//        Flashcard("Flashcard 6 term", "Default definition"),
//        Flashcard("Flashcard 7 term", "Default definition"),
//        Flashcard("Flashcard 8 term", "Default definition"),
//        Flashcard("Flashcard 9 term", "Default definition"),
//        Flashcard("Flashcard 10 term", "Default definition")
//    );
//
//    return flashcards
//}