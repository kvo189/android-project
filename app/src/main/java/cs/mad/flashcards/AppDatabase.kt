package cs.mad.flashcards

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cs.mad.flashcards.entities.*

@Database(entities = [FlashcardSet::class, Flashcard::class], version = 2)
abstract class AppDatabase : RoomDatabase() {
    abstract fun flashcardSetDao() : FlashcardSetDao
    abstract fun flashcardDao() : FlashcardDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "app_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}